import { Stockats.ComPage } from './app.po';

describe('stockats.com App', () => {
  let page: Stockats.ComPage;

  beforeEach(() => {
    page = new Stockats.ComPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
